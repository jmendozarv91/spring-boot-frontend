import { Region } from "./region";
import { Factura } from "../facturas/models/factura";

export class Cliente {
    // tslint:disable-next-line:typedef-whitespace
    id : number;
    nombre: string;
    apellido: string;
    createAt: string ;
    email: string;
    foto: string;
    region:Region;
    facturas: Array<Factura> = [];

}
