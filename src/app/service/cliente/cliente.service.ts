import { Injectable } from '@angular/core';
import {formatDate , DatePipe, registerLocaleData} from '@angular/common';
//internacionalizacion cambio de idioma al proyecto

import { Cliente } from 'src/app/model/cliente.js';
import { Observable, of ,throwError} from 'rxjs';
import { HttpClient, HttpHeaders, HttpRequest , HttpEvent } from '@angular/common/http';
import { map,catchError , tap} from 'rxjs/operators';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Region } from 'src/app/model/region';
import { AuthService } from '../../usuarios/auth.service';


@Injectable()
export class ClienteService {
  // tslint:disable-next-line:whitespace
  private urlEndPoint:string = 'http://localhost:8080/api/clientes';
 // private httpHeader = new HttpHeaders({'Content-type':'application/json'});


 /* private agregarAuthorizationHeader(){
    let token = this.authService.token;
    if(token!=null){
      return this.httpHeader.append('Authorization','Bearer ' + token);
    }
    return this.httpHeader;
  }*/

  private isNoAutorizado(e):boolean{

    if(e.status==401){
      if(this.authService.isAuthenticated()){
        this.authService.logout();
      }

      this.router.navigate(['/login']);
      return true;
    }


    if(e.status==403){
      swal('Acceso Denegado',`Hola ${this.authService.usuario.username} no tiene acceso a este recurso`,'warning');
      this.router.navigate(['/clientes']);
      return true;
    }
    return false;
  }

  constructor(private http: HttpClient , private router:Router , private authService:AuthService) {

  }

  getRegiones():Observable<Region[]>{

    return this.http.get<Region[]>(this.urlEndPoint+'/regiones'/*,{headers:this.agregarAuthorizationHeader()}*/).pipe(
        catchError(e=>{
          this.isNoAutorizado(e);
          return throwError(e);
        })
    );
  }


  // tslint:disable-next-line:typedef-whitespace
  getClientes(page:number) : Observable<any> {
    return this.http.get<Cliente[]>(this.urlEndPoint+'/page/'+page).pipe(
        tap((response:any) => {
           ( response.content as Cliente[]).forEach(
              cliente => {
                  console.log(cliente.nombre);
              }
           )
        }),
        map((response:any)=>{

          (response.content as Cliente[]).map(cliente => {
            cliente.nombre = cliente.nombre.toUpperCase();
            //registerLocaleData(localeES,'es');
            //cliente.createAt = formatDate(cliente.createAt,'dd-MM-yyyy','en-US');
           // let datePipe = new DatePipe('es');
            ///cliente.createAt = datePipe.transform(cliente.createAt , 'EEEE dd,MMMM yyyy');
            ///Personalizando el formato de la fecha
            return cliente;
          } );
          return response;
        }),
        tap(response=>{
          (response.content as Cliente[]).forEach(
            cliente => {
              console.log(cliente.nombre);
            }
          )
        })

    );
    /*return this.http.get(this.urlEndPoint).pipe(
      map((response)=>response as Cliente[]  )
    );*/
  }

  create(cliente:Cliente):Observable<any>{
    return this.http.post<any>(this.urlEndPoint,cliente/*,{headers:this.agregarAuthorizationHeader()}*/).pipe(
       catchError(e=> {


        if(this.isNoAutorizado(e)){
          return throwError(e);
        }


        if(e.status==400){
          return throwError(e);
        }


        swal('Error al crear al cliente',e.error.mensaje,'error');
         return throwError(e);
       })

    );
  }

  getCliente(id):Observable<Cliente>{
    return this.http.get<Cliente>(`${this.urlEndPoint}/${id}`/*,{headers:this.agregarAuthorizationHeader()}*/).pipe(
      catchError(e=>{


        if(this.isNoAutorizado(e)){
          return throwError(e);
        }


        this.router.navigate(['/clientes']);

        swal('Error al editar',e.error.mensaje,'error');
        return throwError(e);
      })

    );
  }

  update(cliente :Cliente):Observable<Cliente>{
    return this.http.put<Cliente>(`${this.urlEndPoint}/${cliente.id}`,cliente/*,{headers:this.agregarAuthorizationHeader()}*/).pipe(
      map((response:any)=> response.cliente as Cliente),
      catchError(e=> {

        if(this.isNoAutorizado(e)){
          return throwError(e);
        }


        if(e.status==400){
          return throwError(e);
        }

        swal(e.error.mensaje,e.error.error,'error');
        return throwError(e);
      })

    );
  }

  delete(id:number):Observable<Cliente>{
    return this.http.delete<Cliente>(`${this.urlEndPoint}/${id}`/*,{headers:this.agregarAuthorizationHeader()}*/).pipe(
      catchError(e=> {

        if(this.isNoAutorizado(e)){
          return throwError(e);
        }



        console.error(e.error.mensaje);
        swal(e.error.mensaje,e.error.error,'error');
        return throwError(e);
      })

    );
  } 

  subirFoto(archivo:File , id):Observable<HttpEvent<{}>>{
    let formData = new FormData();
    formData.append("archivo",archivo);
    formData.append("id",id);

   /* let httpHeaders = new HttpHeaders();
    let token = this.authService.token;
    if(token!=null){
      httpHeaders=httpHeaders.append('Authorization','Bearer '+token);
    }
*/
    const req = new HttpRequest('POST',`${this.urlEndPoint}/upload`,formData,{
        reportProgress:true /*,
        headers:httpHeaders*/
    });
    return this.http.request(req).pipe(
        catchError(e=>{
          this.isNoAutorizado(e);
            return throwError(e);



        })

    );
  }

}
