import { Component, OnInit, Input } from '@angular/core';
import { Cliente } from 'src/app/model/cliente';
import { ClienteService } from '../../service/cliente/cliente.service';
//import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { HttpEventType } from '@angular/common/http';
import { ModalService } from './modal.service';
import { AuthService } from '../../usuarios/auth.service';
import { Factura } from 'src/app/facturas/models/factura';
import { FacturasService } from 'src/app/facturas/services/factura.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'detalle-cliente',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit {
  @Input()
  cliente : Cliente;

  titulo : string ='Detalle del Cliente';
  private fotoSeleccionada:File;
  private progreso : number=0;

  constructor(
    private clienteService:ClienteService,
    private modalService:ModalService ,
    private authService:AuthService , 
    private facturaService:FacturasService
   // , private activateRoute:ActivatedRoute
    ) { }

  ngOnInit() {
    /*this.activateRoute.paramMap.subscribe(params=>{
      let id:number=+params.get('id');
      if(id){
        this.clienteService.getCliente(id).subscribe(cliente=>{
          this.cliente= cliente;
        });
      }

    });*/
  }

  private seleccionarFoto(event){
    this.fotoSeleccionada=event.target.files[0];
    this.progreso = 0 ;
    console.log(this.fotoSeleccionada);
    if(this.fotoSeleccionada.type.indexOf('image')<0){
      swal('Error seleccionar imagen','El archivo debe ser de tipo imagen','error');
      this.fotoSeleccionada=null;
    }
  }

  subirFoto(){

    if(this.fotoSeleccionada){
      this.clienteService.subirFoto(this.fotoSeleccionada,this.cliente.id).subscribe(
        event=>{
            //this.cliente = cliente ;
            if(event.type===HttpEventType.UploadProgress){
              this.progreso = Math.round((event.loaded/event.total)*100);
            }else if(event.type === HttpEventType.Response){
              let response:any = event.body;
              this.cliente = response.cliente as Cliente;
              this.modalService.notificarUpload.emit(this.cliente);

              swal('La foto se ha subido completamente!!',response.mensaje,'success');
            }
           }

    );
    }else{
      swal('Error Upload','Debe seleccionar una foto','error');
    }

  }

  cerrarModal(){
    this.fotoSeleccionada = null;
    this.progreso = 0 ;
    this.modalService.cerrarModal();
  }

  delete(factura):void{
    swal({
      title: '¿Esta seguro?',
      text: `¿Seguro que desea eliminar la factura ${factura.descripcion}`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar',
      cancelButtonText:'No,Cancelar'
    }).then((result) => {
      if (result.value) {
        this.facturaService.delete(factura.id).subscribe(
          response=>{
            this.cliente.facturas = this.cliente.facturas.filter(f=> f!==factura)
            swal(
              'Factura eliminado',
              `Factura ${factura.descripcion} eliminada con exito`,
              'success'
            )
          }

        )


      }
    })
  }



}
