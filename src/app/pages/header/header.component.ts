import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../usuarios/auth.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  // tslint:disable-next-line:no-inferrable-types
  title: string  = 'App Angular';

  constructor(private authService:AuthService , private router:Router) { }

  ngOnInit() {
  }


  logout():void{
    let username = this.authService.usuario.username;
    this.authService.logout();
    swal('Logout',`Hola ${username} ha cerrado sesion con exito`,'success');
    this.router.navigate(['/login']);
  }
}
