import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directiva',
  templateUrl: './directiva.component.html',
  styleUrls: ['./directiva.component.css']
})
export class DirectivaComponent implements OnInit {
 // tslint:disable-next-line:no-inferrable-types
  listaCurso: string[]  = ['typescript', 'Javascript', 'JAVA SE', 'C#', 'PHP'];
  // tslint:disable-next-line:no-inferrable-types
  habilitar: boolean = true;

  constructor() { }

  ngOnInit() {
  }

  setHabilitar(): void {
    // tslint:disable-next-line:whitespace
    this.habilitar = (this.habilitar===true) ? false : true;
  }

}
