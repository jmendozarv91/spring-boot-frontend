import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/model/cliente';
import { ClienteService } from 'src/app/service/cliente/cliente.service';
import { tap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { ModalService } from '../../clientes/detalle/modal.service';
import { AuthService } from '../../usuarios/auth.service';



@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {
  // tslint:disable-next-line:no-inferrable-types
  titulo_header: string = 'Clientes';
  // tslint:disable-next-line:no-inferrable-types
  titulo_title: string = 'Listado de Clientes';
  clientes: Cliente[] ;
  paginador:any;
  clienteSeleccionado:Cliente;

  constructor(
    private _clienteService:ClienteService,
    private activateRoute : ActivatedRoute ,
    private modalService:ModalService,
    private authService:AuthService) {

  }

  //se llama una sola vez cuando se inciie
  ngOnInit() {

    this.activateRoute.paramMap.subscribe(
       params=>{
        let page :number = +params.get('page') ;

            if(!page){
              page = 0;
            }

            this._clienteService.getClientes(page).pipe(
              tap(response=> {
                console.log('Cliente.Component.ts tab#3' + response);
                (response.content as Cliente[]).forEach(cliente=>{
                  console.log(cliente.nombre);
                });
              })
            ).subscribe(
                response=>{
                  this.clientes= response.content as Cliente[];
                  this.paginador = response;
                }
            )

        }
    );

    this.modalService.notificarUpload.subscribe(cliente=>{
     this.clientes = this.clientes.map(clienteOriginal=>{
        if(cliente.id==clienteOriginal.id){
            clienteOriginal.foto = cliente.foto;
        }
        return clienteOriginal;
     })
    });
  }

  delete(cliente:Cliente):void{
    swal({
      title: '¿Esta seguro?',
      text: `¿Seguro que desea eliminar al cliente ${cliente.nombre} ${cliente.apellido}`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar',
      cancelButtonText:'No,Cancelar'
    }).then((result) => {
      if (result.value) {
        this._clienteService.delete(cliente.id).subscribe(
          response=>{
            this.clientes = this.clientes.filter(cli=> cli!==cliente)
            swal(
              'Cliente eliminado',
              `Cliente ${cliente.nombre} eliminado con exito`,
              'success'
            )
          }

        )


      }
    })
  }

  abrirModal(cliente:Cliente){
    this.clienteSeleccionado = cliente ;
    this.modalService.abrirModal();
  }

}
